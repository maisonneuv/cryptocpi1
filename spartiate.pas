(******************************************
    Copyright (c) 2015 Tom Darboux, Elio Maisonneuve
    This file is part of Crypto.

    Crypto is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Crypto is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Crypto.  If not, see <http://www.gnu.org/licenses/>.     
******************************************)


unit spartiate;

interface
function spartiateMain(message:string;cle:string;action:boolean):string;

implementation



uses crt,sysutils;


(******************************
********
** 
** Auteurs	: Elio Maisonneuve, Tom Darboux
** But		: crypter/décrypter grâce a la mèthode de la grille
** 
********
*******************************)

type 
	grille = array of string;
	tab = array of integer;
	ternaire = record
		t1: boolean;
		t2: boolean;
	end;


(******************************
********
** 
** Auteurs 			: Tom Darboux
** But				: Tester si un caractère est un chiffre
** Pré-conditions	: 	
**						'c': caratère alpha numerique.
** Post-conditions	:	
**						renvoie vrai si c'est un chiffre faux sinon.	
********
*******************************)

function chiffre_test(c:char):boolean;
begin
	chiffre_test := ((ord(c) > 47) AND (ord(c) < 58));
end;

(******************************
********
** 
** Auteurs 			: Tom Darboux
** But				: Tester le type d'une chaine, alphabetique ou numerique
** Pré-conditions	: 	
**						'message': chaine de caractère.
** Post-conditions	:	
**						renvoie un ternaire,t1 vaut vrai si c'est une chaine numerique,t2 vaut vrai si c'est une chaine alphabetique.	
********
*******************************)


function type_chaine(message:string):ternaire;
var
	i:integer;
	nombre:boolean;
	mot:boolean;
	test:boolean;
	type01:ternaire;
begin
	nombre:=true;
	mot:=true;
	for i:=1 to length(message) do//pour chaque lettre du message
	begin
		test := chiffre_test(message[i]); // On test si c est un chiffre ou une lettre
		nombre := test AND nombre; // Si ce n est pas un chiffre, alors la clé ne peut pas être un nombre
		mot := (NOT test) AND  mot; // Si il ya un chiffre alors la clé n est pas un mot
	end;
	type01.t1 := nombre;
	type01.t2 := mot;
	type_chaine := type01; 
end;

(******************************
********
** 
** Auteurs 			: Tom Darboux
** But				: transformer le message en un message correct (carre)
** Pré-conditions	: 	
**						'message': chaine de caractère.
						'cle' : entier positif
** Post-conditions	:	
**						renvoie le meme message mais de taille cle².	
********
*******************************)


function correction(message:string;cle:integer):string;
begin
	while(length(message)< cle*cle) do//tant que la longueur est pas ok
		message := message + ' ';//on ajoute un espace a la fin

	correction := message;
end;


(******************************
********
** 
** Auteurs 			: Tom Darboux
** But				: compter le nombre de ligne de la grille
** Pré-conditions	: 	
**						'colonne': nombre de colonnes 
**						'cases' : nombre de cases de la grille.
** Post-conditions	:	
**						renvoie le nombre de ligne sous forme d'un entier.
********
*******************************)


function ligne(colonne,cases:integer):integer;
var
	division:integer;
begin
	division := cases div colonne;
	if (colonne*division <> cases) then division := division+1;//si il ya eu un reste on arrondit au dessus
	ligne := division;
end;

(******************************
********
** 
** Auteurs 			: Tom Darboux
** But				: donner les coordonnés de la colonne d'une case
** Pré-conditions	: 	
**						'x'		  : position dans la phrase 
						'colonne' : nombre de colonne
**						'cases'   : nombre de lettre dans le mot
** Post-conditions	:	
**						renvoie le nombre de ligne sous forme d'un entier.
********
*******************************)


function CColonne(x,colonne,lettre:integer):integer;
begin
	CColonne := x div ligne(colonne,lettre);
end;




(******************************
********
** 
** Auteurs 			: Tom Darboux
** But				: Transformer un message en une grille
** Pré-conditions	: 	
**						'message': une chaine de caractère
**						'cle' : un entier positif.
** Post-conditions	:	
**						renvoie une grille composé du message decoupé en chaine de taille 'cle'
********
*******************************)


function decouper(message:string;cle:integer):grille;
var
i:integer;
t:grille;
mot:string;
begin
	mot := '';
	setlength(t,ligne(cle,length(message)));//la taille du tableau c'est le nombre de ligne de la grille
	for i:=1 to length(message) do//on parcours chaque lettre
	begin
		mot := mot+message[i]; //on ajoute la lettre a la ligne en cours
		if (i mod cle = 0) then//si on est en fin de ligne
		begin
			t[(i div cle)-1] := mot;//on enregistre la ligne
			mot := '';//on remet la ligne a 0
		end;
	end;
	if (mot <> '') then//si la ligne n'est pas enregistré (ligne pas complete)
		t[(i div cle)] := mot;//on l'enregistre
	
	decouper := t;
end;

(******************************
********
** 
** Auteurs 			: Tom Darboux
** But				: renvoyer l'ordre des lettres pour une chaine de caractere
** Pré-conditions	: 	
**						'cle': une chaine de caractère alphabetique soit majuscule soit minuscule où chaque caractere est unique 
** Post-conditions	:	
**						renvoie un tableau d'entier où chaque lettre a ete remplacé par son placement alphabetique par rapport aux autres lettres.
********
*******************************)


function ordre(cle:string):tab;
var
i,j:integer;
t:tab;
begin
	setlength(t,length(cle));

	for i:=1 to length(cle) do//on parcours toutes les lettres
	begin
		t[i-1] := 0;//on considere cette lettre comme la première
		for j:=1 to i-1 do//on parcours toutes les lettres precedentes
		begin
			if (ord(cle[j]) > ord(cle[i])) then//si la lettre precedente est apres dans l'alaphabet
			begin
				t[j-1] := t[j-1] +1;//on augmente la lettre precedente
			end
			else
			begin
				t[i-1] := t[i-1] +1;//on augmente la lettre en cours
			end;
		end;
	end;
	ordre := t;
end;


(******************************
********
** 
** Auteurs 			: Tom Darboux
** But				: renvoyer l'ordre des numeros
** Pré-conditions	: 	
**						't': un tableau d'entier tous different de 0 a n-1 (n est la taille du tableau)
** Post-conditions	:	
**						renvoie un tableau d'entier qui fait la 'transposé' du tableau
********
*******************************)


function ordreinv(t:tab):tab;
var
i,j:integer;
t2:tab;
begin
	setlength(t2,length(t));
	for i:=0 to length(t)-1 do//on parcours touts les nombre possible
		for j:=0 to length(t)-1 do//on parcours tout le tableau
			if (t[j] = i) then t2[i] := j;//lorsque on trouve le nombre, on stocke son emplacement
	ordreinv := t2;		
end;




(******************************
********
** 
** Auteurs 			: Tom Darboux
** But				: renvoie un tableau d'entier de 0 jusque au chiffre demandé
** Pré-conditions	: 	
**						n:entier positif
** Post-conditions	:	
**						renvoie un tableau d'entier composé de 0;1;...;n-1
********
*******************************)


function suite(n:integer):tab;
var
i:integer;
t:tab;
begin
	setlength(t,n);
	for i:=0 to n-1 do//on remplit l'element 0 par 0 le 1er par 1 ... le (n-1)eme par n-1
		t[i] := i;
	
	suite := t;
end;

(******************************
********
** 
** Auteurs 			: Tom Darboux
** But				: generaliser une methode de cryptage avec une clé numerique quelque soit l'ordre des colonnes
** Pré-conditions	: 	
**						'message': une chaine de caractère alphabetique contenant le message à chiffrer
**						'cle' : un entier positif. C'est la clé de chiffrement.
						'placement' : tableau d'entier de taille cle . c'est l'ordre de lecture des colonnes
** Post-conditions	:	
**						renvoie le message crypté grace à la clé et selon l'ordre des colonnes etabli par 'placement'.
********
*******************************)



function cryptage(message:string;cle:integer;placement:tab):string;
var
i,j:integer;
t:grille;
final:string;
begin	
	setlength(t,ligne(cle,length(message)));//la taille du tableau c'est le nombre de ligne de la grille
	t := decouper(message,cle);//on decoupe notre chaine en grille


	final:='';
	for i:=0 to cle-1 do//pour chaque colonne
		for j:=0 to ligne(cle,length(message))-1 do//on parcours chaque ligne 
			final := final + t[j][placement[i]+1];//on ajoute le caractere de  colonne;ligne voulu.
	
	//writeln(final);//on affiche le resultat
	cryptage := final;
end;




(******************************
********
** 
** Auteurs 			: Tom Darboux
** But				: crypter grâce a la mèthode de la grille lorsque la clé est un nombre
** Pré-conditions	: 	
**						'message': une chaine de caractère alphabetique contenant le message à chiffrer
**						'cle' : un entier positif. C'est la clé de chiffrement.
** Post-conditions	:	
**						renvoie le message crypté grace à la clé.
********
*******************************)


function cryptageInt(message:string;cle:integer):string;
var
placement:tab;
begin
	setlength(placement,cle);//on initialise le tableau d'entier
	placement := suite(cle);//on recupere l'odre de chaque lettre de la clé
	cryptageInt := cryptage(message,cle,placement);
end;


(******************************
********
** 
** Auteurs 			: Tom Darboux
** But				: crypter/décrypter grâce a la mèthode de la grille lorsque la clé est un nombre
** Pré-conditions	: 	
**						'message': une chaine de caractère alphabetique contenant le message à chiffrer/déchiffrer 
**						'cle' : un entier positif. C'est la clé de chiffrement/dechiffrement.
**						'action' : un boolean, vrai=crypter le message , faux=décrypter
** Post-conditions	:	
**						renvoie le message décrypté ou crypté selon ce qui a été demandé	
********
*******************************)



function spartiateInt(message:string;cle:integer;action:boolean):string;
var
resultat:string;
begin
	
	resultat := '';
	if (length(message) > cle*cle) then//si le message est trop long
		resultat := spartiateInt(rightStr(message,length(message) - cle*cle),cle,action);//on crypte la fin

	message := LeftStr(message,cle*cle);//on prend le debut
	

	if (length(message) > 0) then
	begin
		message := correction(message,cle);
		if (action) then//si on veut crypter
			spartiateInt := resultat + cryptageInt(message,cle)//on concatene le cryptage du debut et de la fin
		else//si on veut decrypter
			spartiateInt := resultat + cryptageInt(message,cle);//on concatene le cryptage du debut et de la fin
	end
	else
	begin
		spartiateInt := resultat;
	end;

end;


(******************************
********
** 
** Auteurs 			: Tom Darboux
** But				: crypter grâce a la mèthode de la grille lorsque la clé est un mot
** Pré-conditions	: 	
**						'message': une chaine de caractère alphabetique contenant le message à chiffrer 
**						'cle' : un chaine alphabetique. C'est la clé de chiffrement.
** Post-conditions	:	
**						renvoie le message crypté grace à la clé.
********
*******************************)



function cryptageStr(message:string;cle:string):string;
var
placement:tab;
begin
	setlength(placement,length(cle));//on initialise le tableau d'entier
	placement := ordre(cle);//on recupere l'odre de chaque lettre de la clé
	cryptageStr := cryptage(message,length(cle),placement);
end;

(******************************
********
** 
** Auteurs 			: Tom Darboux
** But				: décrypter grâce a la mèthode de la grille lorsque la clé est un mot
** Pré-conditions	: 	
**						'message': une chaine de caractère alphabetique contenant le message à déchiffrer 
**						'cle' : un chaine alphabetique.
** Post-conditions	:	
**						renvoie le message décrypté grace à la clé.
********
*******************************)



function decryptageStr(message:string;cle:string):string;
var
i:integer;
placement:tab;
t:grille;
begin
	setlength(placement,length(cle));
	setlength(t,length(cle));
	placement := ordre(cle);//on recupere l'ordre de chaque lettre
	placement := ordreinv(placement);//on l'inverse car on veut decrypter
	t:=decouper(message,length(cle));//on decoupe le mot en tableau
	message := '';//on remet a 0 le message

	(*on va organiser le message comme pour pouvoir le decrypter avec un nombre*)
	for i:=0 to length(placement)-1 do//pour chaque ligne du tableau
		message := message + t[placement[i]];//on ajoute au message la ligne adequate
		
	decryptageStr:=cryptageInt(message,length(cle));//on crypte le message

end;


(******************************
********
** 
** Auteurs 			: Tom Darboux
** But				: crypter/décrypter grâce a la mèthode de la grille lorsque la clé est un mor
** Pré-conditions	: 	
**						'message': une chaine de caractère alphabetique contenant le message à chiffrer/déchiffrer 
**						'cle' : une chaine de caractère avec des caractere alphabetique tous differents.
**						'action' : un boolean, vrai=crypter le message , faux=décrypter
** Post-conditions	:	
**						renvoie le message décrypté ou crypté selon ce qui a été demandé	
********
*******************************)

function spartiateStr(message:string;cle:string;action:boolean):string;
var
resultat:string;
begin
	message := correction(message,length(cle));
	resultat := '';
	if (length(message) > length(cle)*length(cle)) then//si le message est trop long
		resultat := spartiateStr(rightStr(message,length(message) - (length(cle)*length(cle))),cle,action);//on crypte la fin

	message := LeftStr(message,length(cle)*length(cle));//on prend le debut
	
	if (length(message) > 0) then
	begin
		if (action) then//si on veut crypter
			spartiateStr := resultat + cryptageStr(message,cle)//on concatene le cryptage du debut et de la fin
		else//si on veut decrypter
			spartiateStr := resultat + decryptageStr(message,cle);//on concatene le cryptage du debut et de la fin
	end
	else
	begin
		spartiateStr := resultat;
	end;

end;

(******************************
********
** 
** Auteurs 			: Tom Darboux
** But				: crypter/décrypter grâce a la mèthode de la grille
** Pré-conditions	: 	
**						'message': une chaine de caractère alphabetique contenant le message à chiffrer/déchiffrer 
**						'cle' : une chaine de caractère contenant soit que des caractère numerique soit des caractere alphabetique tous differents. C'est la clé.
**						'action' : un boolean, vrai=crypter le message , faux=décrypter
** Post-conditions	:	
**						renvoie le message décrypté ou crypté selon ce qui a été demandé	
********
*******************************)

function spartiateMain(message:string;cle:string;action:boolean):string;
var
	type_msg : ternaire;
	chiffre : integer;
begin
	type_msg := type_chaine(cle);//on teste le type de la clé

	if (type_msg.t1) then//si c'est un nombre
	begin		
		chiffre := strtoint(cle);//on convertit la clé en integer
		spartiateMain := spartiateInt(message,chiffre,action);//on chiffre/dechiffre avec un clé qui est un nombre	
	end
	else if (type_msg.t2) then//si c'est un mot
	begin
		spartiateMain := spartiateStr(message,cle,action);//on chiffre/dechiffre avec un clé qui est un mot
	end
	else //sinon c'est une clé incorrecte
	begin
		writeln ('Rien à faire!');
		writeln ('La clé est incorrect!');
		spartiateMain := 'erreur';
	end;
end;


finalization
  

end.
