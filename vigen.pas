(******************************************
    Copyright (c) 2015 Tom Darboux, Elio Maisonneuve
    This file is part of Crypto.

    Crypto is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Crypto is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Crypto.  If not, see <http://www.gnu.org/licenses/>.     
******************************************)


unit vigen;

interface
function VigenMain(message:string;cle:string;action:boolean):string;
function VigenMain(message:string;cle:string;action:boolean;alphabett:string):string;




implementation



uses crt,sysutils;

type 
    grille = array of string;
    tab = array of integer;

var
    debutAsc: integer;
    finasc:integer;
    alphabet:string;
    


(******************************
********
** 
** Auteurs          : Tom Darboux
** But              : determiner si un caractere appartient a une chaine de caractere et determiner son emplacement
** Pré-conditions   :   
**                      'carac':un caractere
**                      'mot':une chaine non vide
**                      'emp' : un entier qui sera initialisé
** Post-conditions  :   
**                      renvoie vrai si le caractere existe et initialise dans ce cas la variable emp avec son emplacement et -1 sinon    
********
*******************************)

function rechercher(carac:char;mot:string;var emp:integer):boolean;
var
i:integer;
begin
    emp := -1;
    
    for i:=1 to length(mot) do
    begin
        if (mot[i] = carac) then
        begin
            emp:=i;
            rechercher:=true;
        end
    end;
end;











(******************************
********
** 
** Auteurs          : Tom Darboux
** But              : renvoie le code ascii compatible avec les espaces d'un caractere
** Pré-conditions   :   
**                      'lettre':un caratere presque alphabetique
** Post-conditions  :   
**                      renvoie le code ascii de la lettre sauf pour les caractere ajoutés à l'alphabet     
********
*******************************)
//a=97; espace=32

function ord01(lettre:char):integer;
var
i:integer;
begin
    if (rechercher(lettre,alphabet,i)) then//si il c est un espace ou equivalent
    begin
        ord01 := debutAsc+25+i;//on le place a la fin de l alphabet
        flush(output);//on vide le buffer, pourquoi???? mais au moins ca fonctionne.
    end
    else if (ord(lettre)<debutAsc+26) and (ord(lettre)>=debutAsc) then//sinon si c est une lettre
        ord01 := ord(lettre)//si non on renvoie le code normal
    else
    begin
    	ord01 := -1;
    end;
end;

(******************************
********
** 
** Auteurs          : Tom Darboux
** But              : renvoie le caractere grace au code ascii aussi pour les espace
** Pré-conditions   :   
**                      'a':entier positif
** Post-conditions  :   
**                      renvoie le caractere grace au code ascii sauf pour les caractere de l'alphabet qui seront codés apres le z 
********
*******************************)
//a=97; espace=32

function chr01(a:integer):char;
begin
    if (a > debutAsc+25) and (a<debutAsc+finasc+1) then //si on est juste apres la fin de l alphabet
        chr01 := alphabet[a-debutAsc-25]//on renvoie un la lettre qui correspond dans la suite de l'alphabet
    else if (a<debutAsc+26) and (a>=debutAsc) then//sinon si c est une lettre
        chr01 := chr(a)//on renvoie la lettre
    else
        chr01 := chr(a);//sinon c est un caractere inconnu
end;




(******************************
********
** 
** Auteurs          : Tom Darboux
** But              : transformer chaque lettre d'une chaine en son equivalent numerique (a=0;...)
** Pré-conditions   :   
**                      'message': une chaine de caractère
** Post-conditions  :   
**                      renvoie un tableau d entier qui contient le rang de chaque lettre    
********
*******************************)
//a=97; espace=32

function transform(mot:string):tab;
var
i:integer;
t:tab;
begin
    setlength(t,length(mot));//on créé le tableau d entier
    for i:=1 to length(mot) do//on parcours chaque lettre
    begin
            t[i-1] := ord01(mot[i]) - debutAsc;//on recupere le placement dans l aphabet de la lettre
    end;
    transform := t;
end;

(******************************
********
** 
** Auteurs          : Tom Darboux
** But              : decale une lettre de l'alphabet
** Pré-conditions   :   
**                     'c':caractere alphabetique
**                     'a':entier positifs ou negatif
** Post-conditions  :   
**                      renvoie la lettre de decalé de 'a' places
********
*******************************)


function decalageLettre(c:char;a:integer):char;
begin
        a :=ord01(c) + a;//on lui ajoute le decalage
        a:=a-debutAsc;//on la place dans l alphabet
        a:=a+finasc;//on enpeche que ce soit avant 'a'
        a := a mod (finasc);//on verifie que ca depasse pas le dernier caractere de l'alphabet
        a := a+debutAsc;//on le replace dans le code ascii
        decalageLettre := chr01(a);//on reconvertit en caractere

end;



(******************************
********
** 
** Auteurs          : Tom Darboux
** But              : decale les lettres du mot
** Pré-conditions   :   
**                      'message': une chaine de caractère
                        t : un tabeleau d'enier
** Post-conditions  :   
**                      decale chaque lettre du mot en suivant l'ordre de t
********
*******************************)

function decalageMot(mot:string;t:tab):string;
var
final:string;
i:integer;
begin
    final := '';
    
    for i:=1 to length(mot) do
    begin
        if (ord01(mot[i]) <> -1) then
        begin
            final := final + decalageLettre(mot[i],t[(i-1)mod length(t)]);
           flush(output);//on vide le buffer, pourquoi???? mais au moins ca fonctionne.
        end
        else
            final := final + mot[i];
    end;
    decalageMot := final;
end;

(******************************
********
** 
** Auteurs          : Tom Darboux
** But              : nettoyer la clé de tous les caractères speciaux inconnu
** Pré-conditions   :   
**                      'mot' : une chaine de caractère quelconque non vide
** Post-conditions  :   
**                      renvoie une chaine de caractère avec des caractere de l'alphabet ou ajouté   
********
*******************************)




function nettoyer(mot:string):string;
var
    i:integer;
    mot02:string;
begin
    mot02 := '';
    for i:=1 to length(mot) do
    begin
        if (ord01(mot[i]) <> -1) then
            mot02 := mot02 + mot[i];
    end;
    nettoyer := mot02;
end;

(******************************
********
** 
** Auteurs          : Tom Darboux
** But              : crypter/décrypter grâce a la mèthode de vigenere lorsque la clé est un mot
** Pré-conditions   :   
**                      'message': une chaine de caractère alphabetique contenant le message à chiffrer/déchiffrer 
**                      'cle' : une chaine de caractère alphabetique.
**                      'action' : un boolean, vrai=crypter le message , faux=décrypter
** Post-conditions  :   
**                      renvoie le message décrypté ou crypté selon ce qui a été demandé    
********
*******************************)

function VigenMot(message:string;cle:string;action:boolean):string;
var
t:tab;
i:integer;
begin
    
   cle := nettoyer(cle);
   t := transform(cle);//on transforme la clé en tableau d entier pour recuperer le decalage adequat
   if not(action) then//si on decrypte
        for i:=0 to length(t)-1 do//on parcours tout le tableau de decalage
            t[i] := -t[i];//on l inverse

   VigenMot := decalageMot(message,t);//on decale le message

end;

(******************************
********
** 
** Auteurs          : Tom Darboux
** But              : crypter/décrypter grâce a la mèthode de vigenere lorsque la clé est une lettre
** Pré-conditions   :   
**                      'message': une chaine de caractère alphabetique contenant le message à chiffrer/déchiffrer 
**                      'cle' : une chaine de caractère alphabetique.
**                      'action' : un boolean, vrai=crypter le message , faux=décrypter
** Post-conditions  :   
**                      renvoie le message décrypté ou crypté selon ce qui a été demandé    
********
*******************************)

function VigenLettre(message:string;cle:char;action:boolean):string;
var
i:integer;
final:string;
cle01 : string;
begin
    
    if (action = true) then//si on crypte
    begin
        cle01 := cle+message;//on cocatene le message et la lettre
        for i:=1 to length(cle01) do//on parcours la nouvelle clé
            if (ord01(cle01[i]) = -1) then//si caractère speciale
                cle01[i] := chr(debutAsc);//on le considere comme un 'a'
        vigenLettre := VigenMot(message,cle01,action);//on crypte...
    end
    else
    begin
        final := '';
        for i:=1 to length(message) do//on parcours tous le message
        begin
            if (ord01(message[i]) = -1) or (ord01(cle) = -1) then//Si la caractère servant a décrypter ou le caractere a decrypter est 'inconnu'
                cle := message[i]//on ne décrypte rien
            else                               
                cle := decalageLettre(message[i],-(ord01(cle)-debutAsc));//sinon on le décrypte

            final := final+cle;
        end;
        vigenLettre := final;
    end;
end;

(******************************
********
** 
** Auteurs          : Tom Darboux
** But              : tester la casse d'un caractere
** Pré-conditions   :   
**                      'a':caratere alpahabetique
** Post-conditions  :   
**                      renvoie vrai si le caractere est minuscule    
********
*******************************)


function minuscule(a:char):boolean;
begin
    minuscule := (ord(a) > 96);
end;

(******************************
********
** 
** Auteurs          : Tom Darboux
** But              : crypter/décrypter grâce a la mèthode de vigenere si aucun alphabet n'est specifié
** Pré-conditions   :   
**                      'message': une chaine de caractère alphabetique contenant le message à chiffrer/déchiffrer 
**                      'cle' : une chaine de caractère alphabetique.
**                      'action' : un boolean, vrai=crypter le message , faux=décrypter
** Post-conditions  :   
**                      renvoie le message décrypté ou crypté selon ce qui a été demandé    
********
*******************************)

function VigenMain(message:string;cle:string;action:boolean):string;
begin
    alphabet :=  ' ';//on ne met que l'espace comme car speciale
    finasc := 26+length(alphabet);//on retient l'emplacement du dernier caractere
    if (minuscule(message[1])) then//on teste si le message est en minuscule
    begin
	debutAsc := 97;//si oui on se place au debut de l alphabet minuscule
        message := lowercase(message);//on met tout en minuscule
	cle := lowercase(cle); 
    end	
    else
    begin
        debutAsc := 65;//sinon on se place au debut de l alphabet majuscule
        message := uppercase(message);//on met tout en majuscule
    	cle := uppercase(cle);
    end;
    if(length(cle)>1) then
        VigenMain := vigenMot(message,cle,action)
    else
        VigenMain := vigenLettre(message,cle[1],action);
end;

(******************************
********
** 
** Auteurs          : Tom Darboux
** But              : crypter/décrypter grâce a la mèthode de vigenere si un alphabet est specifié
** Pré-conditions   :   
**                      'message': une chaine de caractère alphabetique contenant le message à chiffrer/déchiffrer 
**                      'cle' : une chaine de caractère alphabetique.
**                      'action' : un boolean, vrai=crypter le message , faux=décrypter
**                      'alphabett' : chaine de caractere non alphabetique
** Post-conditions  :   
**                      renvoie le message décrypté ou crypté selon ce qui a été demandé    
********
*******************************)


function VigenMain(message:string;cle:string;action:boolean;alphabett:string):string;
begin
    
    finasc := 26+length(alphabett);//on retient l'emplacement du dernier caractere
    alphabet := alphabett;//on passe l'alphabet en variable globale
    if (minuscule(message[1])) then//on teste si le message est en minuscule
    begin
    debutAsc := 97;//si oui on se place au debut de l alphabet minuscule
        message := lowercase(message);//on met tout en minuscule
    cle := lowercase(cle); 
    end 
    else
    begin
        debutAsc := 65;//sinon on se place au debut de l alphabet majuscule
        message := uppercase(message);//on met tout en majuscule
        cle := uppercase(cle);
    end;
    if(length(cle)>1) then
        VigenMain := vigenMot(message,cle,action)
    else
        VigenMain := vigenLettre(message,cle[1],action);
end;


finalization
  

end.
