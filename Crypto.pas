(******************************************
    Copyright (c) 2015 Tom Darboux, Elio Maisonneuve
    This file is part of Crypto.

    Crypto is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Crypto is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Crypto.  If not, see <http://www.gnu.org/licenses/>.	
******************************************)

(******************************************
*********
** Auteurs : Elio Maisonneuve, Tom Darboux
** But     : Crypter des messages
** Nom     : Crypto
** Date    : 13/05/2015
** Version : 1.2.1
*********
******************************************)
PROGRAM Crypto;
  uses spartiate, vigen, crt, sysutils;

(*****************************************
*********
** Auteur          : Elio Maisonneuve
** Prototype       : afficherAide(  ):
** Pré-conditions  : Aucune
** Post-conditions : Affiche l'aide et quitte
*********
*****************************************)
PROCEDURE afficherAide ( );
VAR
  frmt : string;
BEGIN
  writeln('Crypto version 1.2.1');
  writeln('Copyright (c) 2015 Tom Darboux, Elio Maisonneuve.');
  writeln('License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>');
  writeln('This is free software: you are free to change and redistribute it.');
  writeln('There is NO WARRANTY, to the extent permitted by law.');
  writeln;
  writeln('Algorithmes supportés : SCYTALE,VIGENERE');
  writeln;
  writeln('Syntaxe : Crypto [-e|-d] -a ALGORITHME -k CLE [-f FICHIER | -m MESSAGE] [-c CHAINE]');
  writeln;
  frmt := '%0:-15s';
  writeln(Format(frmt,[' -e | -d  ']), ': -e pour encoder; -d pour decoder');
  writeln(Format(frmt,[' -a ']), ': Choix de l algorithme de cryptage');
  writeln(Format(frmt,[' -k ']), ': Mot ou lettre pour vigene; mot ou chiffre pour scytale');
  writeln(Format(frmt,[' -f | -m  ']), ': -f fichier avec chemin du fichier contenant le message; -m avec le message');
  writeln(Format(frmt,[' [-c] ']), ': Choix d un jeu de caractères spécial (optionel)');
  writeln(Format(frmt,[' -h, --help']), ': Affiche cette aide et quitte');
  
  
END;

(*****************************************
*********
** Auteurs         : Elio Maisonneuve
** But             : Apelle les fonctions de cryptage/decryptage demandées avec les bons arguments
** Prototype       : appelAlgo( sens, algo, key, message ):boolean
** Pré-conditions  : sens, algo, key, message,alphabet sont des chaine
** Post-conditions : Renvoi vrai si tout c'est bien passé, faux sinon
*********
*****************************************)
FUNCTION appelAlgo ( sens, algo, key, message,alphabet:string ):boolean;
VAR
  intelligent:boolean;//vrai = tout s''est bien passé
BEGIN
  intelligent := true;
  TextColor(RED);
  IF ( sens = 'idiot' ) THEN//si on ne sait pas si on doit crypter ou decrypter alors il ya un probleme
  BEGIN
    writeln('Sens cryptage/decryptage non spécifié');
    intelligent := false;
  END;
  IF ( (algo[1] = '-') OR (key[1] = '-') OR (message[1] = '-') ) THEN//si l''un des arguments commence par un tiret alors il ya un probleme
  BEGIN
    writeln('Problème de paramètre');
    intelligent := false;
  END;

  IF (intelligent) THEN//si tout c''est bien passé
  BEGIN
    TextColor(7); // On remet la couleur de texte par default (lightgrey) avant de passer aux autres algos
    CASE algo OF//on apelle l''algorithme demandé
      'scytale', 'SCYTALE', 
      'spartiate', 'SPARTIATE'  : writeln(spartiateMain(message, key, (sens = '-e')));
      'vigen','VIGEN','VIGENERE','vigenere' : writeln(vigenMain(message, key, (sens = '-e'),alphabet));

      ELSE 
      BEGIN//si l'algorithme n'existe pas il ya un probleme
        TextColor(RED);
        writeln('Algorithme inconnu');
        intelligent := false;
      END;
    END;
  END;

  TextColor(7); // On remet la couleur de texte par default
  appelAlgo := intelligent;
   
END;

(*****************************************
*********
** Auteurs         : Elio Maisonneuve
** But             : lire un fichier externe qui pourra être crypté ou décrypté 
** Prototype       : lectureFichier(sens, algo, key, nomFichier,alphabet:string);
** Pré-conditions  : sens, algo, key, message,nomfichier,alphabet sont des chaine
*********
*****************************************)


PROCEDURE lectureFichier(sens, algo, key, nomFichier,alphabet:string);
VAR
message:string;
fichier:TextFile;
intelligent:boolean;
BEGIN
intelligent := true;
Assign(fichier, nomFichier);//on ouvre le fichier
reset(fichier); //on initaialise le fichier
readln(fichier,message);//on lis la 1ere ligne
  WHILE( (NOT EOF(fichier)) AND intelligent ) DO//tant qu on est pas a la fin du fichier et qu''il n''y a pas d''érreur de paramètre
  BEGIN
    intelligent := appelAlgo(sens, algo, key, message,alphabet);//on crypte/decrypte la ligne en cours
    readln(fichier,message);//on passe a la ligne suivante
  END;

  IF NOT intelligent THEN
    afficherAide();
close(fichier);//on ferme le fichier
END;


VAR
  sens, algo, key, message, nomFichier,alphabet:string;
  i:integer;
BEGIN
  //on initilise les variables avec des valeurs incongrues
  sens := 'idiot';
  key := '-';
  algo := '-';
  message := '-';
  nomFichier := '-';
  alphabet := ' ';


  IF ( ParamCount <> 7) AND (ParamCount <> 9)  THEN //si il n ya pas le bon nombre de paramètre il y a un problème
  BEGIN
    afficherAide();
  END
  ELSE
  BEGIN
	  FOR i:=1 to ParamCount DO//on parcours tous les parametres
	  BEGIN
	    CASE ParamStr(i) OF
	      '-e','-d' : sens := ParamStr(i);
	      '-a'      : algo := ParamStr(i + 1);
	      '-k'      : key := ParamStr(i + 1);
	      '-f'      : nomFichier := (ParamStr(i + 1));
	      '-m'      : message := ParamStr(i + 1);
	      '-c'      : alphabet := ParamStr(i + 1);
	    END;
	  END;

	  IF ( nomFichier <> '-')THEN//si le nom de fichier est initialisé
	  BEGIN
	    IF (FileExists(nomFichier)) THEN//et si le fichier existe
	      lectureFichier(sens, algo, key, nomFichier,alphabet)//on lis le fichier
	    ELSE
	      afficherAide();
	  END
	  ELSE IF NOT (appelAlgo(sens, algo, key, message,alphabet) ) THEN//sinon on utilise le message rentré en paramètre
	    afficherAide(); 
  END;
  
END.

